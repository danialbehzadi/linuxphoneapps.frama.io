+++
title = "Mobile Compatibility: 3"
description = "It's 3 out of 5"
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++

Some parts of the app are not usable with touch input on small screens, even with <code>scale-to-fit</code>.
