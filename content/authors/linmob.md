+++
title = "linmob"
description = "Creator of LinuxPhoneApps.org."
date = 2021-04-01T08:50:45+00:00
updated = 2021-04-01T08:50:45+00:00
draft = false
+++

Creator of **LinuxPhoneApps**.

You can find a little bit of "about me" on my blog [linmob.net](https://linmob.net/about).

I also go by 1peter10 on [github](https://github.com/1peter10) and [framagit](https://framagit.org/1peter10).
