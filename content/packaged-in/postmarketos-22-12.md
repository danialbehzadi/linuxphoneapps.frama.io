+++
title = "postmarketOS 22.12"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

postmarketOS 22.12 was released on 2022-12-18. If you're using postmarketOS 22.12, be a aware that postmarketOS itself only contains some backports for stable service packs and other postmarketOS specific packages (e.g., device kernels and firmware). All other packages can be found in [Alpine 3.17](../alpine-3-17/)
