+++
title = "AUR: Arch User Repository"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

The [Arch User Repository](https://aur.archlinux.org/) contains user generated package build scripts (PKGBUILD), that can be used to easily build additional packages for your Arch Linux and also [Arch Linux ARM](https://archlinuxarm.org) installation. For packages that don't need compiling, see [Arch Linux ARM aarch64](../archlinuxarm-aarch64/) (or [Arch Linux ARM armv7h (for 32bit devices)](../archlinuxarm-armv7h/)).

__Please note:__ [DanctNIX (Arch Linux ARM for PinePhone (Pro) and PineTab)](https://github.com/dreemurrs-embedded/Pine64-Arch/) and [Kupfer](https://kupfer.gitlab.io/) specific packages are not listed here, as they are not indexed on repology. You can check their PKGBUILDs manually: [DanctNIX](https://github.com/dreemurrs-embedded/Pine64-Arch/tree/master/PKGBUILDS); Kupfer [index](https://kupfer.gitlab.io/packages/index.html), [Gitlab](https://gitlab.com/kupfer/packages/pkgbuilds). 

You can also use the AUR with _Manjaro_, but it may not work, packages may fail to build. For Manjaro packages, see [Manjaro stable](../manjaro-stable/) or [Manjaro unstable](../manjaro-unstable/). The following Manjaro PKGBUILDs may be interesting (for Arch users): [Phosh](https://gitlab.manjaro.org/manjaro-arm/packages/community/phosh), [Plasma Mobile](https://gitlab.manjaro.org/manjaro-arm/packages/community/plamo-gear), [Nemo Mobile](https://gitlab.manjaro.org/manjaro-arm/packages/community/nemo-ux), [Core Box](https://gitlab.manjaro.org/manjaro-arm/packages/community/corebox-apps).
