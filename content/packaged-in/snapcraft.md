+++
title = "Snapcraft"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

[Snapcraft](https://snapcraft.io) is the main source of Snaps, Canonical's universal package format. Please note that not all distributions support Snap (e.g., postmarketOS/Alpine do not support it). To use Snaps on your distribution, see [the Snapcraft documentation](https://snapcraft.io/docs/installing-snapd). 
