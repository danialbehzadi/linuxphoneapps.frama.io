+++
title = "postmarketOS 23.06"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

postmarketOS 23.06 was released in June 2023. If you're using postmarketOS 23.06, be a aware that postmarketOS itself only contains some backports for stable service packs and other postmarketOS specific packages (e.g., device kernels and firmware). All other packages can be found in [Alpine 3.18](../alpine-3-18/)
