+++
title = "Debian 11"
date = 2021-08-15T08:50:45+00:00
draft = false
+++
Debian 11 "Bullseye" was first released on August 14th, 2021. 

Offsprings of Debian 11 "Bullseye" include Pure OS "Byzantium" (not indexed on [repology.org](https://repology.org) and thus not listed), and [Devuan 4.0 "Chimaera"](../devuan-4-0/).

You can try to include some packages from newer branches of Debian such as [Debian Unstable/sid](../debian-unstable/) or even [Debian experimental](../debian-experimental/), as explained on the [Mobian Wiki](https://wiki.mobian.org/doku.php?id=tweaks&s[]=unstable#add-sid-repository).

