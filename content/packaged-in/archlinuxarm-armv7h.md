+++
title = "Arch Linux ARM (aarch64)"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

Packages included in Arch Linux ARM for newer 32bit ARM devices (ARMv7h).

See also: [Arch User Repository](../aur/)

