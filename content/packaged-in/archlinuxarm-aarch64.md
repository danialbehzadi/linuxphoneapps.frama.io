+++
title = "Arch Linux ARM (aarch64)"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

Packages included in Arch Linux ARM for 64bit ARM devices (ARMv8+).

See also: [Arch User Repository](../aur/)

__Please note:__ [DanctNIX (Arch Linux ARM for PinePhone (Pro) and PineTab)](https://github.com/dreemurrs-embedded/Pine64-Arch/) and [Kupfer](https://kupfer.gitlab.io/) specific packages are not listed here, as they are not indexed on repology. You can check their PKGBUILDs manually: [DanctNIX](https://github.com/dreemurrs-embedded/Pine64-Arch/tree/master/PKGBUILDS); Kupfer [index](https://kupfer.gitlab.io/packages/index.html), [Gitlab](https://gitlab.com/kupfer/packages/pkgbuilds). 

