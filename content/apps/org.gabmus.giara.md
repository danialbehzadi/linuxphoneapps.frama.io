+++
title = "Giara"
description = "Giara is a reddit app, built with Python, GTK and Handy. Created with mobile Linux in mind."
aliases = []
date = 2020-09-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Gabriele Musco",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Reddit",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "flathub", "gnuguix", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/giara"
homepage = "https://giara.gabmus.org/"
bugtracker = "https://gitlab.gnome.org/World/giara/-/issues/"
donations = "https://liberapay.com/gabmus/donate"
translations = ""
more_information = [ "https://gabmus.org/posts/giara_is_a_reddit_app_for_linux/", "https://linmob.net/2020/10/06/reddit-clients-for-mobile-linux.html",]
summary_source_url = "https://gitlab.gnome.org/World/giara"
screenshots = [ "https://twitter.com/linmobblog/status/1310271585371201537#m",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gabmus.giara"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.giara"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "giara",]
appstream_xml_url = "https://gitlab.gnome.org/World/giara/-/raw/master/data/org.gabmus.giara.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




