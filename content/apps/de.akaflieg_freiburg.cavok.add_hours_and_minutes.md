+++
title = "Add Hours and Minutes"
description = "A simple calculator app that adds times given in hours and minutes"
aliases = []
date = 2021-12-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Akaflieg Freiburg",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "Qt", "Clock", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/Akaflieg-Freiburg/addhoursandminutes"
homepage = "https://akaflieg-freiburg.github.io/addhoursandminutes/"
bugtracker = "https://github.com/Akaflieg-Freiburg/addhoursandminutes/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Akaflieg-Freiburg/addhoursandminutes"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.akaflieg_freiburg.cavok.add_hours_and_minutes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.akaflieg_freiburg.cavok.add_hours_and_minutes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Akaflieg-Freiburg/addhoursandminutes/master/metadata/de.akaflieg_freiburg.cavok.add_hours_and_minutes.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Add Hours and Minutes is a simple calculator app that adds times given in hours and minutes. It helps with the recording of machine running times, with the addition of flight times in your pilots' flight log, or your driving times as a truck driver. The app is free, open source, 100% non-commercial and does not collect user data. [Source](https://github.com/Akaflieg-Freiburg/addhoursandminutes)
