+++
title = "Public Transport"
description = "Bring Real time information for public transportation (currently in Germany) to Linux phones/the Linux desktop"
aliases = []
date = 2021-04-10
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maeve",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/maevemi/publictransport"
homepage = ""
bugtracker = "https://gitlab.com/maevemi/publictransport/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/maevemi/publictransport"
screenshots = [ "https://gitlab.com/maevemi/publictransport/-/tree/master/data/screenshots",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.maevemi.publictransport"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.maevemi.publictransport"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/maevemi/publictransport/-/raw/master/data/com.gitlab.maevemi.publictransport.metainfo.xml.in.in"
reported_by = "Maeve"
updated_by = "script"

+++


### Description

Bring Real time information for public transportation (currently in Germany) to Linux phones/the Linux desktop (the basic features of the likes of DB Navigator or Transportr). [Source](https://gitlab.com/maevemi/publictransport)


### Notice

Rust app, get it from Flatub
