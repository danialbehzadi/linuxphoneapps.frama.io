+++
title = "Social"
description = "A Mastodon and Pleroma application for GNOME"
aliases = []
date = 2020-09-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Christopher Davis",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Mastodon", "Pleroma",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "Chat",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Social"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Social/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Social"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Social"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/World/Social/-/raw/main/data/org.gnome.Social.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Work in progress, login does not work for me yet. No commits (except for translations) since late 2020.