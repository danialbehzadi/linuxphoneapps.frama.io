+++
title = "Phosh Mobile Settings"
description = "Advanced settings for Mobile devices using Phosh"
aliases = []
date = 2022-06-29
updated = 2023-08-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Guido Günther",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_23_05", "nix_unstable", "postmarketos_22.12", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/guido.gunther/phosh-mobile-settings"
homepage = "https://gitlab.gnome.org//guidog/phosh-mobile-settings"
bugtracker = "https://gitlab.gnome.org//guidog/phosh-mobile-settings/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/guido.gunther/phosh-mobile-settings"
screenshots = [ "https://gitlab.gnome.org//guidog/phosh-mobile-settings/-/raw/main/screenshots/welcome.png?inline=false",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/1.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/2.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/3.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/4.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.sigxcpu.MobileSettings.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "phosh-mobile-settings",]
appstream_xml_url = "https://source.puri.sm/guido.gunther/phosh-mobile-settings/-/raw/pureos/byzantium/data/org.sigxcpu.MobileSettings.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

This app allows you to configure some aspects of Phosh that would otherwise
 require command line usage.

[Source](https://source.puri.sm/guido.gunther/phosh-mobile-settings/-/raw/pureos/byzantium/data/org.sigxcpu.MobileSettings.metainfo.xml.in)

### Notice

No more manual fiddling with feedbackd and no more APP ID guessing for scale-to-fit.
