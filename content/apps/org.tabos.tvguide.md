+++
title = "TV Guide"
description = "TV Guide for GNOME"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The Tabos Team",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/tabos/tvguide"
homepage = "https://tabos.gitlab.io/projects/tvguide/"
bugtracker = "https://gitlab.com/tabos/tvguide/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/tvguide"
screenshots = [ "https://tabos.gitlab.io/projects/tvguide/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.tabos.tvguide"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"

+++