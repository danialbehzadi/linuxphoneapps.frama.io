+++
title = "Grock"
description = "Displays geological maps of the UK."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Daniel Burgess",]
categories = [ "geology",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Science", "Geology",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://codeberg.org/dburgess/Grock"
homepage = "http://web.archive.org/web/20220319231530/https://leucoso.me/"
bugtracker = "https://codeberg.org/dburgess/Grock/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/me.leucoso.Grock"
screenshots = [ "https://codeberg.org/dburgess/Grock#screenshots",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "me.leucoso.Grock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.leucoso.Grock"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://codeberg.org/dburgess/Grock/raw/branch/master/data/me.leucoso.Grock.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Display geological maps of the UK, using data from the British Geological Survey. Allows rock unit names, types and ages to be found easily. Linear features and UK-wide gravitational and magnetic anomaly maps can also be shown. [Source](https://flathub.org/apps/me.leucoso.Grock)
