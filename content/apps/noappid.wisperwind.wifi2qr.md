+++
title = "Wifi2QR"
description = "Display QR codes to easily connect to any Wifi for which a NetworkManager connection profile exists."
aliases = []
date = 2021-12-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "wisperwind",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK4",]
backends = [ "networkmanager", "qrencode",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~wisperwind/wifi2qr"
homepage = ""
bugtracker = "https://todo.sr.ht/~wisperwind/wifi2qr"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~wisperwind/wifi2qr"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = "com.example.apps.wifi2qr"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP Wifi QR-Code generator (works fine despite not being designed for mobile - no desktop file)