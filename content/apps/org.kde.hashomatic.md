+++
title = "Hash-o-Matic"
description = "Simple hash validator allowing to compare two files, generate the checksum of a file and verify if a hash matches a file."
aliases = []
date = 2021-12-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "Carl Schwan",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "FileTools",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/carlschwan/hash-o-matic"
homepage = ""
bugtracker = "https://invent.kde.org/carlschwan/hash-o-matic/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/",]
summary_source_url = "https://invent.kde.org/carlschwan/hash-o-matic"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.hashomatic"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/hash-o-matic/-/raw/master/org.kde.hashomatic.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++
