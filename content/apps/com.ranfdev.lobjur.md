+++
title = "Lobjur"
description = "GTK4 client for lobste.rs"
aliases = []
date = 2022-08-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Miglietta",]
categories = [ "news",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Lobste.rs",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Clojure",]
build_systems = [ "nix",]

[extra]
repository = "https://github.com/ranfdev/Lobjur"
homepage = ""
bugtracker = "https://github.com/ranfdev/Lobjur/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/ranfdev/Lobjur"
screenshots = [ "https://raw.githubusercontent.com/ranfdev/Lobjur/master/data/screenshots/1.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.ranfdev.Lobjur"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.ranfdev.Lobjur"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/ranfdev/Lobjur/master/data/com.ranfdev.Lobjur.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Simple client for lobste.rs online community. From the website: "Lobsters is a computing-focused community centered around link aggregation and discussion, launched on July 3rd, 2012" [Source](https://raw.githubusercontent.com/ranfdev/Lobjur/master/data/com.ranfdev.Lobjur.metainfo.xml)
