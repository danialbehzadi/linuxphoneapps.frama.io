+++
title = "DieBahn"
description = "Travel with all your train information in one place"
aliases = []
date = 2022-03-22
updated = 2023-08-15

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "hafas-rest",]
services = [ "hafas",]
packaged_in = [ "alpine_edge", "aur", "nix_unstable", "flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]

[extra]
repository = "https://gitlab.com/schmiddi-on-mobile/diebahn"
homepage = "https://mobile.schmidhuberj.de/diebahn"
bugtracker = "https://gitlab.com/schmiddi-on-mobile/diebahn/-/issues"
donations = "https://gitlab.com/schmiddi-on-mobile/diebahn#donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/Schmiddiii/diebahn/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml"
screenshots = [ "https://gitlab.com/schmiddi-on-mobile/diebahn/blob/master/data/screenshots/overview.png?raw=true", "https://gitlab.com/schmiddi-on-mobile/diebahn/blob/master/data/screenshots/search.png?raw=true", "https://gitlab.com/schmiddi-on-mobile/diebahn/blob/master/data/screenshots/journeys.png?raw=true", "https://gitlab.com/schmiddi-on-mobile/diebahn/blob/master/data/screenshots/journey.png?raw=true",]
screenshots_img = []
all_features_touch = 1
intended_for_mobile = 1
app_id = "de.schmidhuberj.DieBahn"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.DieBahn"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "diebahn",]
appstream_xml_url = "https://gitlab.com/schmiddi-on-mobile/diebahn/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description
DieBahn lets you look up travel information for many different railways, all without needing to navigate through different websites.


* Search for journeys
* View the details of a journey including departure, arrivals, delays, platforms
* Adapative for small screens
* Bookmark a search or a journey
* Show more information like prices
* Many different search profiles

[Source](https://gitlab.com/schmiddi-on-mobile/diebahn/-/raw/master/data/de.schmidhuberj.DieBahn.metainfo.xml)

