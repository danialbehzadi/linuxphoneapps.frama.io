+++
title = "Run Free!"
description = "Running log app built for Phosh with some basic analysis of your running schedule"
aliases = []
date = 2021-09-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "greenbeast",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://gitlab.com/greenbeast/run-free"
homepage = ""
bugtracker = "https://gitlab.com/greenbeast/run-free/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/run-free"
screenshots = [ "https://gitlab.com/greenbeast/run-free/-/tree/master/Screenshots",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "greenbeast"
updated_by = "script"

+++