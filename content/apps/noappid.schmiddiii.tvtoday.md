+++
title = "tvtoday"
description = "A application to inspect the television program of today. Made for the pinephone."
aliases = []
date = 2021-07-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "schmiddiii",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "TV Spielfilm",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/Schmiddiii/tvtoday"
homepage = ""
bugtracker = "https://github.com/Schmiddiii/tvtoday/issues/"
donations = ""
translations = ""
more_information = [ "https://github.com/Schmiddiii/tvtoday/wiki",]
summary_source_url = "https://github.com/Schmiddiii/tvtoday"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1420807553357864968",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

This application is still in development, but should work. This application currently only supports TV SPIELFILM, but can be extended to work on any website serving similar content. [Source](https://github.com/Schmiddiii/tvtoday)


### Notice

WIP, works, limited to German TV currently, but adding other providers of tv information is documented at https://github.com/Schmiddiii/tvtoday/wiki/Creating-Providers