+++
title = "Readerview"
description = "A mobile-ready gpu-accelerated gemini browser."
aliases = []
date = 2021-05-13
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "gemini browser",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://sr.ht/~martijnbraam/Readerview/"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://sr.ht/~martijnbraam/Readerview/"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1392920004442591233",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.postmarketos.readerview"
scale_to_fit = "org.postmarketos.readerview"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Part of the UI vanish when on Gemini sites, even with scale-to-fit applied. To build it, use a version of gmni before OpenSSL was replace with BearSSL.