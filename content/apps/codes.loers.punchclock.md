+++
title = "Punchclock"
description = "Track time for your tasks."
aliases = []
date = 2023-02-24

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "loers",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "Office", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo", "make",]

[extra]
repository = "https://gitlab.com/floers/punchclock"
homepage = ""
bugtracker = "https://gitlab.com/floers/punchclock/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/codes.loers.Punchclock"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/codes.loers.punchclock/1.png", "https://img.linuxphoneapps.org/codes.loers.punchclock/2.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "codes.loers.Punchclock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/codes.loers.Punchclock"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = ""

+++
