+++
title = "Huely"
description = "Control WiFi lights using this simple app."
aliases = []
date = 2022-03-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ben Foote",]
categories = [ "smart home",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK3", "granite", "libhandy",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/benpocalypse/Huely"
homepage = ""
bugtracker = "https://github.com/benpocalypse/Huely/issues/"
donations = ""
translations = ""
more_information = [ "https://twitter.com/benpocalypse/status/1496282293731827714",]
summary_source_url = "https://github.com/benpocalypse/Huely"
screenshots = [ "https://raw.githubusercontent.com/benpocalypse/Huely/main/screenshots/en/light-and-dark.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.benpocalypse.Huely"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.benpocalypse.Huely"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/benpocalypse/Huely/main/data/Huely.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

This app is intended to control WiFi connected lights that are compatible with the Android Magic Home app. Additional light types will most likely be added in the future, but for now this is the initial target light type. [Source](https://github.com/benpocalypse/Huely)


### Notice

Almost a 5 - the app is just a few pixels to wide, and is should be usable without scale-to-fit.
