+++
title = "PassUi"
description = "PassUi is a free and open source graphical user interface for the standard unix password manager written in python, with small screens in mind."
aliases = []
date = 2021-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "micke",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "WxWidgets",]
backends = [ "pass",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://code.smolnet.org/micke/passui"
homepage = ""
bugtracker = "https://code.smolnet.org/micke/passui/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://code.smolnet.org/micke/passui"
screenshots = [ "https://github.com/mickenordin/passui",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++