+++
title = "Syncthing GTK"
description = "GUI and notification area icon for Syncthing"
aliases = []
date = 2023-08-07


[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "kozec",]
categories = [ "file transfer", "file sync",]
mobile_compatibility = [ "2", ]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = [ "Syncthing",]
services = [ "Syncthing",]
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "flathub", "gnuguix", "pureos_landing",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://github.com/syncthing-gtk/syncthing-gtk"
homepage = "https://github.com/syncthing-gtk/syncthing-gtk/"
bugtracker = "https://github.com/syncthing-gtk/syncthing-gtk/issues"
donations = "https://liberapay.com/kozec"
translations = "https://www.transifex.com/syncthing-gtk/syncthing-gtk/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/org.syncthing-gtk.syncthing-gtk.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/doc/syncthing-gtk.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "me.kozec.syncthingtk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.kozec.syncthingtk"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/me.kozec.syncthingtk/master/me.kozec.syncthingtk.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "syncthing-gtk",]
appstream_xml_url = "https://raw.githubusercontent.com/syncthing-gtk/syncthing-gtk/main/org.syncthing-gtk.syncthing-gtk.appdata.xml"
reported_by = "Guido Günther"

+++

### Description
Syncthing is a continuous file synchronization program. It synchronizes files
between two or more computers in real time, safely protected from prying eyes.
This is a GTK frontend to syncthing.

### Notice

Previously, the program was developed at [https://github.com/kozec/syncthing-gtk](https://github.com/kozec/syncthing-gtk).
