+++
title = "Reactions"
description = "GIF search for GNOME desktop and mobile devices."
aliases = []
date = 2022-03-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alastair Toft",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Giphy",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Zig",]
build_systems = [ "meson",]

[extra]
repository = "https://codeberg.org/atoft/Reactions"
homepage = ""
bugtracker = "https://codeberg.org/atoft/Reactions/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/atoft/Reactions"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "dev.atoft.Reactions"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.atoft.Reactions"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://codeberg.org/atoft/Reactions/raw/branch/main/data/dev.atoft.Reactions.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Reactions is written in Zig and uses GTK4 and libadwaita. It uses the GIPHY search API, but is intended to support multiple API providers. [Source](https://codeberg.org/atoft/Reactions)


### Notice

Gifs/content does not display successfully (tested with Flathub build on Librem 5/PureOS Byzantium)
