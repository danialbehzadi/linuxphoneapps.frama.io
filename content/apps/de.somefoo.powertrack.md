+++
title = "Powertrack"
description = "Tool to track battery statistics."
aliases = []
date = 2023-03-25

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Pit Henrich",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/somefoo/Powertrack"
homepage = "https://github.com/somefoo/Powertrack"
bugtracker = "https://github.com/somefoo/Powertrack/issues"
donations = ""
translations = ""
more_information = [ "https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3CCAMBPg2d%3D7oke53D1Vzxr%3DPR+akwrV5kpuJTuyakS3sX-Gdw8zQ%40mail.gmail.com%3E",]
summary_source_url = "https://raw.githubusercontent.com/somefoo/Powertrack/master/data/de.somefoo.powertrack.appdata.xml"
screenshots = [ "https://user-images.githubusercontent.com/50917034/227716797-ee2f86f2-4acb-4fec-9e63-1d7c2f482664.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.somefoo.powertrack/1.png", "https://img.linuxphoneapps.org/de.somefoo.powertrack/2.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.somefoo.powertrack"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/somefoo/Powertrack/master/data/de.somefoo.powertrack.appdata.xml"
reported_by = "somefoo"
updated_by = ""

+++


### Description

An App to help track the power consumption on the Pinephone (Pro). It helps to estimate the remaining time on the current battery charge. [Source](https://github.com/somefoo/Powertrack)


### Notice

Confirmed to work on PinePhone and PinePhone Pro on postmarketOS edge and v22.12, [experimental APKBUILD](https://framagit.org/linmobapps/apkbuilds/-/tree/master/Powertrack). Not working on OnePlus 6.