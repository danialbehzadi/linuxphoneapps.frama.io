+++
title = "Whale"
description = "Whale is actually a very simple file explorer using Kirigami"
aliases = []
date = 2021-12-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Carl Schwan",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "System", "FileTools", "FileManager",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/carlschwan/whale"
homepage = ""
bugtracker = "https://invent.kde.org/carlschwan/whale/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/",]
summary_source_url = "https://carlschwan.eu/2021/12/18/more-kde-apps/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.whale"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP fun project, worked well when tested on 2021-12-18.
