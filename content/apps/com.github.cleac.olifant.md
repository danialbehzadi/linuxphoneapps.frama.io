+++
title = "Olifant (Fork for phones)"
description = "A fork of Olifant to make it kind of pleasant to use on the linux phone."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "alexcleac",]
categories = [ "social media",]
mobile_compatibility = [ "3",]
status = [ "inactive",]
frameworks = [ "GTK3", "granite",]
backends = []
services = [ "Mastodon",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Alexmitter/olifant"
homepage = ""
bugtracker = "https://github.com/Alexmitter/olifant/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Alexmitter/olifant"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=olifant",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.cleac.olifant"
scale_to_fit = "com.github.cleac.olifant"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Alexmitter/olifant/master/data/com.github.cleac.olifant.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

No commits in June 2020, therefore marked inactive.