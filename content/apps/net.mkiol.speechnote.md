+++
title = "Speech Note"
description = "Note taking and reading with Speech to Text and Text to Speech"
aliases = []
date = 2023-06-25

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michal Kosciesza",]
categories = [ "note taking", "text to speech", "speech to text",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick", "Silica",]
backends = [ "Coqui STT", "Vosk", "whisper.cpp", "espeak-ng", "MBROLA", "Piper", "RHVoice", "Coqui TTS",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Qt", "AudioVideo",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/mkiol/dsnote"
homepage = "https://github.com/mkiol/dsnote"
bugtracker = "https://github.com/mkiol/dsnote/issues"
donations = ""
translations = "https://app.transifex.com/mkiol/dsnote"
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/screenshot_1.png", "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/screenshot_2.png", "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/screenshot_3.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/net.mkiol.speechnote/1.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/2.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/3.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.mkiol.SpeechNote"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.mkiol.SpeechNote"
flatpak_link = "https://raw.githubusercontent.com/mkiol/dsnote/main/flatpak/net.mkiol.SpeechNote.yaml"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml"
reported_by = "linmob"

+++

### Description
Speech Note enables you to take and read notes with your voice with multiple languages.
 It uses Speech to Text and Text to Speech conversions to do so.
 All voice processing is entirely done off-line, locally on your
 computer without the use of a network connection. Your privacy is
 always respected. No data is sent to the Internet.

[Source](https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml)

