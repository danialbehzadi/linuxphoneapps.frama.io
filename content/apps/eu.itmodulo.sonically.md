+++
title = "Sonically"
description = "Native client implementing subsonic API (audio streaming) using opus"
aliases = []
date = 2021-11-13
updated = 2022-09-30

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "Sonically Logo License",]
app_author = [ "linuxonmobile",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4",]
backends = [ "portaudio", "libnotify",]
services = [ "Subsonic",]
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "Audio",]
programming_languages = [ "go",]
build_systems = [ "go",]

[extra]
repository = "https://git.itmodulo.eu/LinuxOnMobile/Sonically"
homepage = "https://projects.itmodulo.eu/portfolio/sonically/"
bugtracker = "https://git.itmodulo.eu/LinuxOnMobile/Sonically/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.itmodulo.eu/LinuxOnMobile/Sonically"
screenshots = [ "https://projects.itmodulo.eu/portfolio/sonically/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "eu.itmodulo.Sonically"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "BigB"
updated_by = "BigB"

+++


### Description

Is subsonic / navidrome client made for mobile Linux. It is optimized for outdoor conditions e.g. to work without stable internet connection. It uses opus for streaming. [Source](https://itmodulo.eu/#projects)


### Notice

Not available in any repos yet, binaries with checksums available on repository.