+++
title = "alrisha"
description = "an experimental Qt/QML destkop browser for Gemini"
aliases = []
date = 2021-05-13
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "fabrixxm",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]

[extra]
repository = "https://git.sr.ht/~fabrixxm/alrisha"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://git.sr.ht/~fabrixxm/alrisha/tree/master/item/alrisha-desktop/README.md",]
summary_source_url = "https://git.sr.ht/~fabrixxm/alrisha/"
screenshots = [ "https://fosstodon.org/@linmob/106228565439331658",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

No icon or launcher, a bit buggy, but it works!
