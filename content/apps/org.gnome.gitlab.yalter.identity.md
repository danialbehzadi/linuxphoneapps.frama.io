+++
title = "Identity"
description = "Compare images and videos"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Ivan Molodetskikh",]
categories = [ "image and video comparison",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Video", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/YaLTeR/identity"
homepage = ""
bugtracker = "https://gitlab.gnome.org/YaLTeR/identity/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.gitlab.YaLTeR.Identity/",]
summary_source_url = "https://gitlab.gnome.org/YaLTeR/identity"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.gitlab.YaLTeR.Identity"
scale_to_fit = "identity"
flathub = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.Identity"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "identity",]
appstream_xml_url = "https://gitlab.gnome.org/YaLTeR/identity/-/raw/master/data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++






### Description

A program for comparing multiple versions of an image or video. [Source](https://gitlab.gnome.org/YaLTeR/identity)


### Notice

Too wide in portrait, fine after scale-to-fit
