+++
title = "LLs Video Player (Plasma Mobile version)"
description = "Video Player for Plasma Mobile"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge",]
freedesktop_categories = [ "Qt", "KDE", "Video", "Player",]
programming_languages = [ "Javascript", "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/jbbgameich/videoplayer"
homepage = ""
bugtracker = "https://invent.kde.org/jbbgameich/videoplayer/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/jbbgameich/videoplayer"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.mobile.vplayer"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plasma-videoplayer",]
appstream_xml_url = "https://invent.kde.org/jbbgameich/videoplayer/-/raw/master/org.kde.mobile.vplayer.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++





### Description

Plasma Mobile Version of LLs Video Player [Source](https://invent.kde.org/jbbgameich/videoplayer)


### Notice

Last commit in February 2020.
