+++
title = "pods"
description = "Cross platform Podcast app targetting the pinephone specifically (in early development)"
aliases = []
date = 2021-05-30
updated = 2022-12-23

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "dskleingeld",]
categories = [ "podcast client",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "iced",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Network", "Audio", "Feed", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/dvdsk/pods"
homepage = ""
bugtracker = "https://github.com/dvdsk/pods/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/dvdsk/pods"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP, interface works once you get used to it. Window is not placed optimally in Phosh, hiding the title bar and showing a slice of the wallpaper.