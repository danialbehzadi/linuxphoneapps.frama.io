+++
title = "Shortwave"
description = "Listen to internet radio"
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Felix Häcker",]
categories = [ "internet radio",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Shortwave"
homepage = "https://gitlab.gnome.org/World/Shortwave"
bugtracker = "https://gitlab.gnome.org/World/Shortwave/-/issues/"
donations = ""
translations = ""
more_information = [ "https://blogs.gnome.org/haeckerfelix/2021/04/09/new-shortwave-release/", "https://apps.gnome.org/app/de.haeckerfelix.Shortwave/",]
summary_source_url = "https://gitlab.gnome.org/World/Shortwave"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=shortwave",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.haeckerfelix.Shortwave"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.haeckerfelix.Shortwave"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "shortwave", "gnome-shortwave",]
appstream_xml_url = "https://gitlab.gnome.org/World/Shortwave/-/raw/main/data/de.haeckerfelix.Shortwave.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++






### Description

Shortwave is an internet radio player that provides access to a station database with over 25,000 stations. [Source](https://flathub.org/apps/de.haeckerfelix.Shortwave)
