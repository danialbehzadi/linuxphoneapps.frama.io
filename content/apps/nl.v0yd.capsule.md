+++
title = "Capsule"
description = "Medication tracker for GNOME"
aliases = []
date = 2023-02-19
updated = 2023-03-04

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jonas Dreßler",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/verdre/Capsule"
homepage = ""
bugtracker = "https://gitlab.gnome.org/verdre/Capsule/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/verdre/Capsule"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/nl.v0yd.capsule/1.png", "https://img.linuxphoneapps.org/nl.v0yd.capsule/2.png", "https://img.linuxphoneapps.org/nl.v0yd.capsule/3.png", "https://img.linuxphoneapps.org/nl.v0yd.capsule/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "nl.v0yd.Capsule"
scale_to_fit = "Capsule"
flathub = "https://flathub.org/apps/nl.v0yd.Capsule"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/verdre/Capsule/-/raw/main/data/nl.v0yd.Capsule.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"

+++


### Description

Schedule and track your medication intake. Note: This app does not run in the background or send reminder notifications. [Source](https://gitlab.gnome.org/verdre/Capsule/-/raw/main/data/nl.v0yd.Capsule.metainfo.xml.in)


### Notice

Mobile friendly since 1.1, [issue tracking mobile compatibility](https://gitlab.gnome.org/verdre/Capsule/-/issues/6).
