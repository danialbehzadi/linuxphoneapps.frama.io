+++
title = "VVave"
description = "VVAVE will handle your whole music collection by retrieving semantic information from the web."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "Qt", "Audio", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/vvave"
homepage = "https://mauikit.org/apps/vvave/"
bugtracker = "https://invent.kde.org/maui/vvave/-/issues/"
donations = ""
translations = ""
more_information = [ "https://medium.com/@temisclopeolimac/its-vvave-c3f83da90380",]
summary_source_url = "https://vvave.kde.org/"
screenshots = [ "https://medium.com/@temisclopeolimac/its-vvave-c3f83da90380", "https://medium.com/nitrux/maui-apps-apk-packages-5c966f185f0c", "https://medium.com/nitrux/maui-kde-fcdc920138e2", "https://medium.com/nitrux/maui-plasma-mobile-sprint-2019-c20031700b3b",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.vvave"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.vvave"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "vvave",]
appstream_xml_url = "https://invent.kde.org/maui/vvave/-/raw/master/org.kde.vvave.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++



