+++
title = "Moment"
description = "Customizable and keyboard-operable Matrix client"
aliases = []
date = 2022-04-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Moment contributors",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick",]
backends = [ "matrix-nio", "libolm",]
services = [ "Matrix",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "QML", "Python",]
build_systems = [ "qmake",]

[extra]
repository = "https://gitlab.com/mx-moment/moment"
homepage = "https://mx-moment.xyz/"
bugtracker = "https://gitlab.com/mx-moment/moment/-/issues/"
donations = ""
translations = ""
more_information = [ "https://gitlab.com/mx-moment/moment/-/blob/main/docs/INSTALL.md", "https://gitlab.com/mx-moment/moment/-/blob/main/docs/MIRAGEDIFF.md", "https://gitlab.com/mx-moment/moment/-/blob/main/docs/CHANGELOG.md",]
summary_source_url = "https://gitlab.com/mx-moment/moment/-/raw/main/packaging/moment.metainfo.xml"
screenshots = [ "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m05-main-pane-small.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m06-chat-small.png", "https://gitlab.com/mx-moment/moment/-/raw/main/docs/screenshots/m07-room-pane-small.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "xyz.mx_moment.moment"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.mx_moment.moment"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "moment",]
appstream_xml_url = "https://gitlab.com/mx-moment/moment/-/raw/main/packaging/moment.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++






### Description

A fancy, customizable, keyboard-operable Matrix chat client for encrypted and decentralized communication. Written in Qt/QML and Python, currently in alpha. [Source](https://gitlab.com/mx-moment/moment)
