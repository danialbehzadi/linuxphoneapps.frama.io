+++
title = "Pipeline"
description = "Watch YouTube, LBRY, and PeerTube videos in one place"
aliases = []
date = 2021-03-31
updated = 2023-08-16

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "YouTube", "lbry", "PeerTube",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Video", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/schmiddi-on-mobile/pipeline"
homepage = "https://mobile.schmidhuberj.de/pipeline/"
bugtracker = "https://gitlab.com/schmiddi-on-mobile/pipeline/issues"
donations = "https://gitlab.com/schmiddi-on-mobile/pipeline#donate"
translations = ""
more_information = [ "https://www.tubefeeder.de/wiki/different-player.html",]
summary_source_url = "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/de.sch    midhuberj.tubefeeder.metainfo.xml"
screenshots = [ "https://gitlab.com/schmiddi-on-mobile/pipeline/blob/master/data/screenshots/feed.png?raw=true", "https://gitlab.com/schmiddi-on-mobile/pipeline/blob/master/data/screenshots/watch_later.png?raw=true", "https://gitlab.com/schmiddi-on-mobile/pipeline/blob/master/data/screenshots/filters.png?raw=true", "https://gitlab.com/schmiddi-on-mobile/pipeline/blob/master/data/screenshots/subscriptions.png?raw=true",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/1.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/2.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/3.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/4.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/5.png",]
all_features_touch = true
intended_for_mobile = 1
app_id = "de.schmidhuberj.tubefeeder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.tubefeeder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tubefeeder",]
appstream_xml_url = "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/de.schmidhuberj.tubefeeder.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description
Pipeline lets you watch and download videos from YouTube,
 LBRY and PeerTube, all without needing to navigate through
 different websites.

Pipeline comes with several features:

* Subscribe to channels
* Play videos with any video player
* Filter out unwanted videos in the feed
* Import subscriptions from NewPipe or YouTube
* Multiple platforms

[Source](https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/de.schmidhuberj.tubefeeder.metainfo.xml)

### Notice

Make sure to read the [wiki](http://www.tubefeeder.de/wiki/). Ported to GTK4/libadwaita with release 1.6.0, renamed from Tubefeeder to Pipeline in July 2023 with v1.11.0.
