+++
title = "PGallery"
description = "A dead simple photo gallery made for Pinephone. Built with Vala and Gtk"
aliases = []
date = 2020-12-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "joancipria",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/joancipria/pgallery"
homepage = ""
bugtracker = "https://github.com/joancipria/pgallery/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/joancipria/pgallery"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.joancipria.pgallery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Nascent, but works nicely already.