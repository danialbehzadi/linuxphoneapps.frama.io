+++
title = "Glide"
description = "Linux/macOS media player based on GStreamer and GTK+"
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "The Glide Team",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "AudioVideo", "Video", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/philn/glide"
homepage = ""
bugtracker = "https://github.com/philn/glide/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/philn/glide"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.baseart.Glide"
scale_to_fit = "glide"
flathub = "https://flathub.org/apps/net.baseart.Glide"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/philn/glide/master/data/net.baseart.Glide.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++
