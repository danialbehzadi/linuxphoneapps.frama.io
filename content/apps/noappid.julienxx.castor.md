+++
title = "Castor"
description = "A graphical client for plain-text protocols written in Rust with GTK. It currently supports the Gemini, Gopher and Finger protocols."
aliases = []
date = 2021-02-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "julienxx",]
categories = [ "gemini browser",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "gnuguix", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "make",]

[extra]
repository = "https://git.sr.ht/~julienxx/castor"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~julienxx/castor"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "castor-browser",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++







### Notice

Works!