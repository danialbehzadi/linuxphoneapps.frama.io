+++
title = "Save the bunny!"
description = "Save the bunny! is a turn-based puzzle game."
aliases = []
date = 2019-03-17

[taxonomies]
project_licenses = [ "Apache-2.0",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/pabloalba/savethebunny"
homepage = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=es.seastorm.merlin"
screenshots = [ "https://play.google.com/store/apps/details?id=es.seastorm.merlin",]
screenshots_img = []
app_id = "es.seastorm.merlin"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
