+++
title = "FreeOrion"
description = "Turn-based space empire and galactic conquest game."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-2.0-only AND CC-BY-SA-3.0",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "pureos_landing",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/freeorion/freeorion"
homepage = "https://freeorion.org/index.php/Main_Page"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "org.freeorion.FreeOrion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.freeorion.FreeOrion"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "freeorion",]
appstream_xml_url = "https://raw.githubusercontent.com/freeorion/freeorion/master/packaging/org.freeorion.FreeOrion.metainfo.xml"
+++



