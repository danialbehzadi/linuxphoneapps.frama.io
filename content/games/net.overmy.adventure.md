+++
title = "Quicky in search of Mickey"
description = "In a beautiful magical forest magic live animals. Among them are our friends Quicky and Mickey. But once Mickey got lost. What happened to him? Quicky will have to find his brother and unravel the mysteries of the magic forest."
aliases = []
date = 2019-03-17

[taxonomies]
project_licenses = [ "not specified",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/cyberbach/Adventure"
homepage = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=net.overmy.adventure"
screenshots = [ "https://play.google.com/store/apps/details?id=net.overmy.adventure",]
screenshots_img = []
app_id = "net.overmy.adventure"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
