+++
title = "Arkade"
description = "Collection of Arcade games developed in Kirigami"
aliases = []
date = 2020-10-15

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game launcher",]
mobile_compatibility = [ "5",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable", "opensuse_tumbleweed",]

[extra]
reported_by = "linmob"
verified = "❎"
repository = "https://invent.kde.org/games/arkade"
homepage = ""
more_information = []
summary_source_url = "https://invent.kde.org/games/arkade"
screenshots = []
screenshots_img = []
app_id = "org.kde.gamecenter"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "arkade",]
appstream_xml_url = "https://invent.kde.org/games/arkade/-/raw/master/org.kde.arkade.appdata.xml"
+++


