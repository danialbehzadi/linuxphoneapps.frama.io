+++
title = "Shootin Stars"
description = "Round based endless runner style space shooter"
aliases = []
date = 2021-12-02

[taxonomies]
project_licenses = [ "MIT",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "Godot",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "Hank"
verified = "✅"
repository = "https://gitlab.com/greenbeast/shootin-stars"
homepage = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/shootin-stars"
screenshots = [ "https://gitlab.com/greenbeast/shootin-stars",]
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++



### Notice

Built for the PinePhone and runs just fine