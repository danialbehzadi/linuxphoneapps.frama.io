+++
title = "Plasma Samegame"
description = "Samegame clone suitable for mobile devices"
aliases = []
date = 2020-09-07

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "linmob"
verified = "✅"
repository = "https://invent.kde.org/plasma-mobile/plasma-samegame"
homepage = ""
more_information = []
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-samegame"
screenshots = [ "https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/20200906_22h15m48s_grim.jpg",]
screenshots_img = []
app_id = "org.kde.samegame"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-samegame/-/raw/master/org.kde.samegame.appdata.xml"

+++