+++
title = "New apps of LinuxPhoneApps.org, Q4/2022"
date = 2023-02-26T19:00:00+01:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["linmob"]

+++

It's almost March, and here, finally, is the post about the apps that have been added in the last quarter of 2022.
<!-- more -->
### New apps

The following 6 apps where added in Q4 of 2022:

#### October

* [Endeavour](https://linuxphoneapps.org/apps/org.gnome.todo/), an intuitive and powerful application to manage your personal tasks.
- [GNOME Files (Nautilus)](https://linuxphoneapps.org/apps/org.gnome.nautilus/), a file browser for GNOME.
- [Flare](https://linuxphoneapps.org/apps/de.schmidhuberj.flare/), an unofficial Signal GTK4 client.

#### November

* [Tooth](https://linuxphoneapps.org/apps/dev.geopjr.tooth/), a GTK4 Mastodon Client - Fork of Tootle.
* [Frog](https://linuxphoneapps.org/apps/com.github.tenderowl.frog/), an utility to extract text from anywhere.

#### Dezember

* [Wince](https://linuxphoneapps.org/apps/space.quietfeathers.wince/) provides search for restaurants and businesses with a GTK4/libadwaita UI powered by Yelp. _Thanks, wildeyedskies, for creating and adding this app!_

### Maintenance

Some maintenance happened, to make maintenance more easy, we now have a [set of scripts](https://framagit.org/linmobapps/linmobapps.frama.io#checks) - help with running the scripts and fixing issues (many need manual intervention) is welcome!

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org!

If you want to contribute an/your app or work on the website itself, please check the [FAQ](@/docs/help/faq.md#join-the-effort)![^1] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org) or subscribe/post to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss)!


[^1]: If you are not aware of any new apps, check out apps listed in [apps-to-be-added](https://linuxphoneapps.org/lists/apps-to-be-added/), test the app and add it!
