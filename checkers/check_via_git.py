#!/usr/bin/env python3

import asyncio
import pathlib
import re
import sys
import tempfile

import aiofiles
import appstream_python
import frontmatter
import git
import httpx

import utils


def is_sourcehut(url):
    return re.match(r"^https?://(?:[^.]+\.)?sr.ht/", url) is not None


def is_qt(url):
    return re.match(r"^https?://code.qt.io/", url) is not None


def get_repository_url(repo):
    if is_sourcehut(repo):
        return re.sub(r"^https?://(?:[^.]+\.)?sr\.ht/(.*)$", r"https://git.sr.ht/\g<1>", repo).lower(), None

    if is_qt(repo):
        return re.sub(r"^https?://code\.qt\.io/(?:cgit/)?(.*\.git).*$", r"https://code.qt.io/\g<1>", repo), None

    branch = None
    if (m := re.search(r"(?:/-)?/tree/(?P<branch>.*)$", repo)) is not None:  # Github, Gitlab
        repo = repo[: m.start()]
        branch = m.groupdict().get("branch")

    if repo[-1] == "/":
        repo = repo[:-1]

    return re.match(r"^(?:[^.]*(?:\.(?!git))?)*", repo)[0] + ".git", branch


async def find_appstream_xml(repo_url, branch=None, repo_path="repo"):
    try:
        kwargs = {}
        if branch is not None:
            kwargs["branch"] = branch
        repo = git.Repo.clone_from(repo_url, repo_path, multi_options=["--depth 1"], **kwargs)

        def pred(x, y):
            if x.type != "blob":
                return False

            name = pathlib.Path(x.path).name
            if "metainfo.xml" not in name and "appdata.xml" not in name:
                return False

            return True

        return [x.path for x in repo.head.commit.tree.traverse(pred)]
    except Exception as e:
        print(f"Error checking git repository {repo_url} for AppStream file: {e}", file=sys.stderr)
        return None


async def load_appstream(file):
    if not file:
        return None
    app = appstream_python.AppstreamComponent()
    try:
        async with aiofiles.open(file, mode="rb") as f:
            app.load_bytes(await f.read())
    except Exception as e:
        print(f"Error loading {file}: {e}", file=sys.stderr)
        return None
    return app


async def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")
    if utils.get_recursive(item, "extra.appstream_xml_url", "").strip():
        return False

    repo, branch = get_repository_url(utils.get_recursive(item, "extra.repository", ""))
    if not repo:
        print(f"No repository specified for {item_name}", file=sys.stderr)
        return False
    print(f"No AppStream url found for {item_name}, checking {repo}, {branch=}", file=sys.stderr)

    async with aiofiles.tempfile.TemporaryDirectory() as tmpdir:
        repo_path = pathlib.Path(tmpdir) / "repo"
        appstream_xml_files = await find_appstream_xml(repo, branch, repo_path)
        appstream_xml_files = [appstream_xml_file for appstream_xml_file in (appstream_xml_files or []) if await load_appstream(repo_path / appstream_xml_file)]
        if appstream_xml_files:
            print(f"Found AppStream metainfo files for {item_name} with {repo} at paths {', '.join(appstream_xml_files)}")
            return True

    return False


async def check_file(client, filename, update=False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found = await check(client, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(folder, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        for filename in folder.glob("**/*.md"):
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FOLDER")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_folder = pathlib.Path(sys.argv[2])
    found = await run(apps_folder, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_folder}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
